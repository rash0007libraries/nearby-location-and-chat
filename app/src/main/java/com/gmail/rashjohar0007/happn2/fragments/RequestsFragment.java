package com.gmail.rashjohar0007.happn2.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.adapters.RequestResponseAdapters;
import com.gmail.rashjohar0007.happn2.basic.UserLikes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RequestsFragment extends BaseFragment {
    private RequestResponseAdapters adapter;
    private List<String> allrequests;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout_request;
    public RequestsFragment() {
    }
    public static RequestsFragment newInstance() {
        RequestsFragment fragment = new RequestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_requests, container, false);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,false);
        recyclerView=(RecyclerView) v.findViewById(R.id.list_requests);
        recyclerView.setLayoutManager(layoutManager);
        allrequests=new LinkedList<String>();
        adapter=new RequestResponseAdapters(getActivity(),allrequests);
        recyclerView.setAdapter(adapter);
        refreshLayout_request = (SwipeRefreshLayout) v.findViewById(R.id.refreshView_requests);
        refreshLayout_request.setColorSchemeColors(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));
        refreshLayout_request.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Initilize();

            }
        });
        Initilize();
        return v;
    }

    private void Initilize() {
        final UserLikes like=new UserLikes(FirebaseAuth.getInstance().getCurrentUser().getUid());
        like.getReceiver(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> data=(ArrayList<String>)dataSnapshot.getValue();
                if(data==null) {
                    return;
                }
                for (String id:data) {
                    if(!allrequests.contains(id)) {
                        allrequests.add(id);
                    }
                }
                adapter.notifyDataSetChanged();
                refreshLayout_request.setRefreshing(false);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                refreshLayout_request.setRefreshing(false);
            }
        });
    }
}
