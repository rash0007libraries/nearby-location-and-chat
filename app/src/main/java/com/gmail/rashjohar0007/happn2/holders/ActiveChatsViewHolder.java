package com.gmail.rashjohar0007.happn2.holders;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.views.AppCompatSquareImageView;


public class ActiveChatsViewHolder extends RecyclerView.ViewHolder {
    public CardView action_nearby_chat_start;
    public AppCompatTextView item_person_name;
    public AppCompatSquareImageView item_person_image;

    public ActiveChatsViewHolder(View v) {
        super(v);
        item_person_name=(AppCompatTextView) v.findViewById(R.id.item_person_name);
        action_nearby_chat_start= (CardView) v.findViewById(R.id.action_nearby_chat_start);
        item_person_image= (AppCompatSquareImageView) v.findViewById(R.id.item_person_image);
    }
}