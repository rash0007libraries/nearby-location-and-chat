package com.gmail.rashjohar0007.happn2.basic;

import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

@IgnoreExtraProperties
public class UserLikes extends UserLikesAbstract {

    public UserLikes() {
        super();
    }

    public UserLikes(String senderId) {
        super(senderId);
    }
}