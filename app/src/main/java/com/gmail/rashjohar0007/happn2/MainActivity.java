package com.gmail.rashjohar0007.happn2;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.gmail.rashjohar0007.happn2.adapters.MainTabsAdapter;
import com.gmail.rashjohar0007.happn2.basic.MyApp;
import com.gmail.rashjohar0007.happn2.fragments.RequestsFragment;
import com.gmail.rashjohar0007.happn2.fragments.MyChatFragment;
import com.gmail.rashjohar0007.happn2.fragments.NearbyFragment;
import com.gmail.rashjohar0007.happn2.interfaces.OnFragmentInteractionListener;
import com.gmail.rashjohar0007.happn2.models.FragmentedTitle;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnFragmentInteractionListener,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private MainTabsAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;
    private List<FragmentedTitle> fragments;
    private FirebaseAuth mAuth;
    private FirebaseUser user = null;
    protected GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
            startActivity(intent);
        } else {
            user = mAuth.getCurrentUser();
        }
        mGoogleApiClient= MyApp.mGoogleApiClient;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragments=new LinkedList<FragmentedTitle>();
        fragments.add(new FragmentedTitle("Nearby", NearbyFragment.newInstance()));
        fragments.add(new FragmentedTitle("My Chats", MyChatFragment.newInstance()));
        fragments.add(new FragmentedTitle("Requests", RequestsFragment.newInstance()));
        mSectionsPagerAdapter = new MainTabsAdapter(getSupportFragmentManager(),fragments);
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if(id==R.id.action_signout) {
            mAuth.signOut();
            if(!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }
}
