package com.gmail.rashjohar0007.happn2.basic;

import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by Johars on 1/14/2017.
 */
@IgnoreExtraProperties
public abstract class UserLocation implements Serializable {
    protected String userid="";
    protected GeoLocation location;
    public UserLocation() {

    }

    public UserLocation(String userid, GeoLocation location) {
        this.userid = userid;
        this.location = location;
    }

    public GeoLocation getLocation() {
        return location;
    }

    public void setLocation(GeoLocation location) {
        this.location = location;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
}
