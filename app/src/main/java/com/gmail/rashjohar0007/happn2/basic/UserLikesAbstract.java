package com.gmail.rashjohar0007.happn2.basic;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@IgnoreExtraProperties
public class UserLikesAbstract implements Serializable,IUserLikes{
    private String senderId;
    private ArrayList<String> receiverId;
    @Exclude
    private Boolean isConstructed = false;
    @Exclude
    private DatabaseReference myref= FirebaseDatabase.getInstance().getReference(UserLikes.class.getSimpleName());
    public UserLikesAbstract() {
    }

    public void getReceiver(ValueEventListener action) {
        myref.child(this.senderId).addValueEventListener(action);
    }
    public UserLikesAbstract(String senderId) {
        this.senderId=senderId;
    }

    public Boolean getConstructed() {
        return isConstructed;
    }

    public void setConstructed(Boolean constructed) {
        isConstructed = constructed;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public ArrayList<String> getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(ArrayList<String> receiverId) {
        this.receiverId = receiverId;
    }

    @Override
    public void RespondAccept(String key,DatabaseReference.CompletionListener action) {
        if(receiverId.contains(key)) {
            receiverId.remove(key);
        }
        myref.child(this.senderId).setValue(receiverId, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                UsersChats chat=new UsersChats(key, FirebaseAuth.getInstance().getCurrentUser().getUid());
                chat.SaveChat(action);
            }
        });

    }

    @Override
    public void RespondReject(String key,DatabaseReference.CompletionListener action) {
        if(receiverId.contains(key)) {
            receiverId.remove(key);
        }
        myref.child(this.senderId).setValue(receiverId,action);
    }

    @Override
    public void SendLike(String rec, DatabaseReference.CompletionListener action) {
        if(!receiverId.contains(rec)){
            receiverId.add(rec);
            myref.child(this.senderId).setValue(receiverId, action);
        }
    }
}
