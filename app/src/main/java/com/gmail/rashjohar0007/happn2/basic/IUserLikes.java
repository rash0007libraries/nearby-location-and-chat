package com.gmail.rashjohar0007.happn2.basic;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

public interface IUserLikes {
    public void RespondAccept(String key,DatabaseReference.CompletionListener action);
    public void RespondReject(String key,DatabaseReference.CompletionListener action);
    public void SendLike(String rec,DatabaseReference.CompletionListener action);
}
