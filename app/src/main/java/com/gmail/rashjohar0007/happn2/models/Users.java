package com.gmail.rashjohar0007.happn2.models;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;

@IgnoreExtraProperties
public class Users implements Serializable {

    private String uid;
    private String displayname;
    private String email;
    private String photourl;
    private String token;
    @Exclude
    public static DatabaseReference refer= FirebaseDatabase.getInstance().getReference().child(Users.class.getSimpleName());
    public Users() {
    }

    public Users(String uid, String displayname, String email, String photourl,String token) {
        this.uid = uid;
        this.displayname = displayname;
        this.email = email;
        this.photourl = photourl;
        this.token=token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }
    @Exclude
    public static void getUserInfo(String key, ValueEventListener action) {
        refer.child(key).addValueEventListener(action);
    }
}
