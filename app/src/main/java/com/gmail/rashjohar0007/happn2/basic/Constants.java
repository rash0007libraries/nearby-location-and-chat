package com.gmail.rashjohar0007.happn2.basic;

import android.os.AsyncTask;

import com.gmail.rashjohar0007.happn2.services.MyFirebaseMessagingService;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;

public class Constants {
    public static final String INSTANCE_ID_TOKEN_RETRIEVED = "iid_token_retrieved";
    public static final String TAG = "Happn2";
    public static final String FRIENDLY_MSG_LENGTH = "friendly_msg_length";
    public static final String getChannelName(String firstId, String secondId)
    {
        if(firstId.hashCode()>secondId.hashCode())
        {
            return firstId+secondId;
        } else if(firstId.hashCode()<secondId.hashCode()) {
            return secondId+firstId;
        }else {
            return firstId.hashCode()+""+secondId.hashCode();
        }
    }
    public static final String MESSAGE_TYPE_LIKE="Like";
    public static final String MESSAGE_TYPE_ACCEPT="Accept";
    public static final String MESSAGE_TYPE_REJECT="Reject";
    public static final String MESSAGE_TYPE_CHAT="Chat";
    public static final void SendNotification(String to, String messagetype) throws JSONException {
        JSONObject message=new JSONObject();
        JSONObject notification=new JSONObject();
        if(messagetype.equals(MESSAGE_TYPE_LIKE)) {
            message.put("message",FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" Liked You.") ;
            notification.put("body",FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" Liked You.") ;
            notification.put("title","New Like") ;
        }else if(messagetype.equals(MESSAGE_TYPE_ACCEPT)) {
            message.put("message",FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has accepted your Like Request.") ;
            notification.put("body",FirebaseAuth.getInstance().getCurrentUser().getDisplayName()+" has accepted your Like Request.") ;
            notification.put("title","Like Accepted") ;
        } else {
            message.put("message",messagetype) ;
            notification.put("body",messagetype) ;
            notification.put("title","New Message") ;
        }
        message.put("notification",notification);
        JSONObject data=new JSONObject();
        try {
            data.put("data",message);
            data.put("to",to);
            post("https://fcm.googleapis.com/fcm/send",data.toString());
        }  catch (IOException e) {

        }

        /*RemoteMessage.Builder remoteMessage= new RemoteMessage.Builder(to);
        remoteMessage.addData(to,message);
        FirebaseMessaging.getInstance().send(remoteMessage.build());*/
    }
    public static void post(String url, String json) throws IOException {

        AsyncTask<String,String,String> task=new AsyncTask<String,String,String>() {
            @Override
            protected String doInBackground(String[] objects) {
                try {
                    MediaType JSON
                            = MediaType.parse("application/json; charset=utf-8");
                    OkHttpClient client = new OkHttpClient();
                    RequestBody body = RequestBody.create(JSON, json);
                    Request request = new Request.Builder()
                            .url(url)
                            .post(body)
                            .header("Authorization","key=AIzaSyB9wX01olPHpt1IBamvY7HB2iBQx0_xNu0")
                            .build();
                    Response response = null;
                    response = client.newCall(request).execute();
                    return response.body().string();
                } catch (IOException e) {
                    return null;
                }

            }

            @Override
            protected void onPostExecute(String s) {
                if(s!=null) {

                }
            }
        };
        task.execute();

    }
    public static String run(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }

}
