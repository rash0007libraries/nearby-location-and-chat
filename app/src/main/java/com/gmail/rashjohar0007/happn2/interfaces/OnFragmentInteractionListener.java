package com.gmail.rashjohar0007.happn2.interfaces;

import android.net.Uri;

public interface OnFragmentInteractionListener {
    public void onFragmentInteraction(Uri uri);
}