package com.gmail.rashjohar0007.happn2.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.gmail.rashjohar0007.happn2.MainActivity;
import com.gmail.rashjohar0007.happn2.models.FragmentedTitle;

import java.util.List;

public class MainTabsAdapter extends FragmentStatePagerAdapter {
    private List<FragmentedTitle> fragments;

    public MainTabsAdapter(FragmentManager fm,List<FragmentedTitle> fragments) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragments.get(position).getTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position).getFragment();
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}