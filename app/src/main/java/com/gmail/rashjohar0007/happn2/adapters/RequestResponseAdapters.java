package com.gmail.rashjohar0007.happn2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.basic.Constants;
import com.gmail.rashjohar0007.happn2.basic.UserLikes;
import com.gmail.rashjohar0007.happn2.holders.RequestResponseViewHolder;
import com.gmail.rashjohar0007.happn2.models.Users;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class RequestResponseAdapters extends RecyclerView.Adapter<RequestResponseViewHolder> {
    private Context context;
    private List<String> allpersons;
    private DatabaseReference myRef=FirebaseDatabase.getInstance().getReference();

    public RequestResponseAdapters(Context context, List<String> allpersons) {
        this.context = context;
        this.allpersons = allpersons;
    }

    @Override
    public RequestResponseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(context).inflate(R.layout.item_layout_requests,parent,false);
        return new RequestResponseViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RequestResponseViewHolder holder, int position) {
        String item=allpersons.get(position);
        myRef.child(Users.class.getSimpleName()).child(item).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Users user = dataSnapshot.getValue(Users.class);
                if(user.getPhotourl()!=null) {
                    Glide.with(context)
                            .load(user.getPhotourl())
                            .into(holder.item_person_image);
                }else {
                    holder.item_person_image.setImageResource(R.drawable.ic_person_white_48dp);
                }
                holder.item_person_name.setText(user.getDisplayname());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        holder.item_action_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final UserLikes like=new UserLikes(FirebaseAuth.getInstance().getCurrentUser().getUid());
                like.getReceiver(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ArrayList<String> data=(ArrayList<String>)dataSnapshot.getValue();
                        ArrayList<String> receiverId=new ArrayList<>();
                        if(data==null) {
                            like.setReceiverId(receiverId);
                            return;
                        }

                        for (String id:data) {
                            receiverId.add(id);
                        }
                        like.setReceiverId(receiverId);
                        like.RespondAccept(item, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                allpersons.remove(item);
                                notifyDataSetChanged();
                                Users.getUserInfo(item, new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        try {
                                            Constants.SendNotification(dataSnapshot.getValue(Users.class).getToken(),Constants.MESSAGE_TYPE_ACCEPT);
                                        } catch (JSONException e) {

                                        }

                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        });

                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        holder.item_action_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final UserLikes like=new UserLikes(FirebaseAuth.getInstance().getCurrentUser().getUid());
                like.getReceiver(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ArrayList<String> data=(ArrayList<String>)dataSnapshot.getValue();
                        ArrayList<String> receiverId=new ArrayList<>();
                        if(data==null) {
                            like.setReceiverId(receiverId);
                            return;
                        }

                        for (String id:data) {
                            receiverId.add(id);
                        }
                        like.setReceiverId(receiverId);
                        like.RespondReject(item, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                allpersons.remove(item);
                                notifyDataSetChanged();
                                Users.getUserInfo(item, new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        });
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });
    }

    @Override
    public int getItemCount() {
        return allpersons.size();
    }
}