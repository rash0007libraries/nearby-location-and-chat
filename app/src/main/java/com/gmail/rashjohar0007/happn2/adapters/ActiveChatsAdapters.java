package com.gmail.rashjohar0007.happn2.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.gmail.rashjohar0007.happn2.ChatActivity;
import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.basic.UsersChats;
import com.gmail.rashjohar0007.happn2.holders.ActiveChatsViewHolder;
import com.gmail.rashjohar0007.happn2.models.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class ActiveChatsAdapters extends RecyclerView.Adapter<ActiveChatsViewHolder> {
    private Context context;
    private List<UsersChats> allpersons;
    private DatabaseReference myRef=FirebaseDatabase.getInstance().getReference();

    public ActiveChatsAdapters(Context context, List<UsersChats> allpersons) {
        this.context = context;
        this.allpersons = allpersons;
    }

    @Override
    public ActiveChatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(context).inflate(R.layout.item_layout_active_chat,parent,false);
        return new ActiveChatsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ActiveChatsViewHolder holder, int position) {
        UsersChats item=allpersons.get(position);
        myRef.child(Users.class.getSimpleName()).child(item.getUser2()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Users user = dataSnapshot.getValue(Users.class);
                if(user.getPhotourl()!=null) {
                    Glide.with(context)
                            .load(user.getPhotourl())
                            .into(holder.item_person_image);
                }else {
                    holder.item_person_image.setImageResource(R.drawable.ic_person_white_48dp);
                }
                holder.item_person_name.setText(user.getDisplayname());
                holder.action_nearby_chat_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent=new Intent(context, ChatActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("chatId",item.getChatId());
                        bundle.putSerializable("user2",user);
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return allpersons.size();
    }
}