package com.gmail.rashjohar0007.happn2.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.holders.NearByPersonViewHolder;
import com.gmail.rashjohar0007.happn2.models.NearByPersonEntity;

import java.util.List;

public class NearbyPersonAdapters extends RecyclerView.Adapter<NearByPersonViewHolder> {
    private Context context;
    private int layout;
    private List<NearByPersonEntity> allpersons;

    public NearbyPersonAdapters(Context context, int layout, List<NearByPersonEntity> allpersons) {
        this.context = context;
        this.layout = layout;
        this.allpersons = allpersons;
    }

    @Override
    public NearByPersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(context).inflate(layout,parent,false);
        return new NearByPersonViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NearByPersonViewHolder holder, int position) {
        NearByPersonEntity item=allpersons.get(position);
        if(item.getImageUrl()!="") {
            Glide.with(context)
                    .load(item.getImageUrl())
                    .into(holder.item_person_image);
        }else {
            holder.item_person_image.setImageResource(R.drawable.ic_person_white_48dp);
        }
        holder.item_person_name.setText(item.getDisplayName());
        if(item.getActionEnabled()) {
            holder.item_person_action.setEnabled(true);
        } else {
            holder.item_person_action.setEnabled(false);
        }
        if(item.getChatAllowed()) {
            holder.item_person_action.setImageResource(R.drawable.ic_chat_light_blue_900_48dp);
            holder.item_person_action.setOnClickListener(item.getAction());
        } else {
            holder.item_person_action.setImageResource(R.drawable.ic_favorite_red_600_48dp);
            holder.item_person_action.setOnClickListener(item.getAction());
        }
    }

    @Override
    public int getItemCount() {
        return allpersons.size();
    }
}