package com.gmail.rashjohar0007.happn2.fragments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.adapters.ActiveChatsAdapters;
import com.gmail.rashjohar0007.happn2.adapters.RequestResponseAdapters;
import com.gmail.rashjohar0007.happn2.basic.UserLikes;
import com.gmail.rashjohar0007.happn2.basic.UsersChats;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyChatFragment extends BaseFragment {

    public MyChatFragment() {
    }
    public static MyChatFragment newInstance() {
        MyChatFragment fragment = new MyChatFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }
    private ActiveChatsAdapters adapter;
    private List<UsersChats> allchats;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout_mychat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_my_chat, container, false);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,false);
        recyclerView=(RecyclerView) v.findViewById(R.id.list_requests);
        recyclerView.setLayoutManager(layoutManager);
        allchats=new LinkedList<UsersChats>();
        adapter=new ActiveChatsAdapters(getActivity(),allchats);
        recyclerView.setAdapter(adapter);
        refreshLayout_mychat = (SwipeRefreshLayout) v.findViewById(R.id.refreshView_mychat);
        refreshLayout_mychat.setColorSchemeColors(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));
        refreshLayout_mychat.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Initilize();
            }
        });
        Initilize();
        return v;
    }

    private void Initilize() {
        UsersChats chats=new UsersChats(FirebaseAuth.getInstance().getCurrentUser().getUid());
        chats.getUsersChats().addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                allchats.clear();
                for (DataSnapshot item:dataSnapshot.getChildren()) {
                    UsersChats chat=new UsersChats();
                    chat.setUser1(FirebaseAuth.getInstance().getCurrentUser().getUid());
                    chat.setUser2(item.getKey());
                    chat.setChatId(item.getValue(String.class));
                    allchats.add(chat);
                    adapter.notifyDataSetChanged();
                    refreshLayout_mychat.setRefreshing(false);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                refreshLayout_mychat.setRefreshing(false);
            }
        });
    }
}
