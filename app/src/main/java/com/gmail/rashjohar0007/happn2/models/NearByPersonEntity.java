package com.gmail.rashjohar0007.happn2.models;

import android.view.View;

public class NearByPersonEntity {

    private String userid ="";
    private String displayName ="";
    private String ImageUrl="";
    private View.OnClickListener action;



    private Boolean isActionEnabled=true;
    private Boolean isChatAllowed=false;

    public NearByPersonEntity(String userid, String displayName, String imageUrl) {
        this.userid = userid;
        this.displayName = displayName;
        ImageUrl = imageUrl;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public View.OnClickListener getAction() {
        return action;
    }

    public void setAction(View.OnClickListener action) {
        this.action = action;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Boolean getChatAllowed() {
        return isChatAllowed;
    }

    public void setChatAllowed(Boolean chatAllowed) {
        isChatAllowed = chatAllowed;
    }
    public Boolean getActionEnabled() {
        return isActionEnabled;
    }

    public void setActionEnabled(Boolean actionEnabled) {
        isActionEnabled = actionEnabled;
    }
}