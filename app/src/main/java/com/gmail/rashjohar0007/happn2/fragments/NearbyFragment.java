package com.gmail.rashjohar0007.happn2.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;
import com.gmail.rashjohar0007.happn2.ChatActivity;
import com.gmail.rashjohar0007.happn2.MainActivity;
import com.gmail.rashjohar0007.happn2.R;
import com.gmail.rashjohar0007.happn2.SignInActivity;
import com.gmail.rashjohar0007.happn2.adapters.NearbyPersonAdapters;
import com.gmail.rashjohar0007.happn2.basic.Constants;
import com.gmail.rashjohar0007.happn2.basic.MyApp;
import com.gmail.rashjohar0007.happn2.basic.UserLikes;
import com.gmail.rashjohar0007.happn2.basic.UserLocationUpdate;
import com.gmail.rashjohar0007.happn2.basic.UsersChats;
import com.gmail.rashjohar0007.happn2.models.NearByPersonEntity;
import com.gmail.rashjohar0007.happn2.models.Users;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class NearbyFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String REQUESTING_LOCATION_UPDATES_KEY = "Requesting Location Update";
    private static final int LOCATION_SETTINGS_CHANGE_KEY = 133;
    private FirebaseAuth mAuth;
    private FirebaseUser user = null;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation = null;
    protected int REQUEST_CHECK_SETTINGS = 0;
    protected RecyclerView list_nearbyperson;
    protected List<NearByPersonEntity> allpersons;
    protected NearbyPersonAdapters adapter;
    protected DatabaseReference myRef;
    protected FusedLocationProviderClient mFusedLocationClient;
    protected LocationRequest mLocationRequest;
    protected Boolean mRequestingLocationUpdates = false;
    protected LocationCallback mLocationCallback;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private SwipeRefreshLayout refreshLayout_nearby;

    public NearbyFragment() {
    }
    public static NearbyFragment newInstance() {
        NearbyFragment fragment = new NearbyFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_nearby, container, false);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            Intent intent = new Intent(getActivity(), SignInActivity.class);
            startActivity(intent);
        } else {
            user = mAuth.getCurrentUser();
        }
        mGoogleApiClient = MyApp.mGoogleApiClient;

        myRef = FirebaseDatabase.getInstance().getReference();
        list_nearbyperson = (RecyclerView) v.findViewById(R.id.list_nearbypersons);
        refreshLayout_nearby = (SwipeRefreshLayout) v.findViewById(R.id.refreshView_nearby);
        refreshLayout_nearby.setColorSchemeColors(
                getResources().getColor(R.color.colorAccent),
                getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimaryDark));
        list_nearbyperson.setLayoutManager(new GridLayoutManager(getActivity(),2,
                GridLayoutManager.VERTICAL, false));
        allpersons = new LinkedList<NearByPersonEntity>();
        adapter = new NearbyPersonAdapters(getActivity(),
                R.layout.item_layout_nearby_person, allpersons);
        list_nearbyperson.setAdapter(adapter);
        updateValuesFromBundle(savedInstanceState);
        checkLocationPermission();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(getActivity());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

            }
        }).addOnFailureListener(getActivity(), new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statuscode = ((ApiException) e).getStatusCode();
                switch (statuscode) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(getActivity(), LOCATION_SETTINGS_CHANGE_KEY);
                        } catch (IntentSender.SendIntentException e1) {

                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });

        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return v;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                onLocationChanged(location);
            }
        });
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                for (Location location : locationResult.getLocations()) {
                    onLocationChanged(location);
                }
            }
        };
        refreshLayout_nearby.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onLocationChanged(mLastLocation);
            }
        });
        return v;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        startLocationUpdates();
                    }

                } else {
                }
                return;
            }

        }
    }
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getActivity())
                        .setTitle("Location Off")
                        .setMessage("Device location is off.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }



    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        if(mRequestingLocationUpdates)
        {
            startLocationUpdates();
        }
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            stopLocationUpdates();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(mRequestingLocationUpdates)
        {
            startLocationUpdates();
        }
        if (ContextCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            startLocationUpdates();
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,mRequestingLocationUpdates);
        super.onSaveInstanceState(outState);
    }
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(REQUESTING_LOCATION_UPDATES_KEY);
            }
        }
    }

    private void stopLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.removeLocationUpdates( mLocationCallback);
    }
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }




    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private Boolean IsAdditioAllowed(String key) {
        if(mAuth.getCurrentUser().getUid().equals(key)) {
            return false;
        }
        for (NearByPersonEntity person:allpersons) {
            if(person.getUserid().equals(key))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (user != null) {
            UserLocationUpdate userlocation = new UserLocationUpdate(mAuth.getCurrentUser().getUid(), location);
            userlocation.ChangeLocation(getActivity(), new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String keybase, DatabaseError error) {
                    allpersons.clear();
                    userlocation.FindNearByUser(getActivity(), 0.02, new GeoQueryEventListener() {
                        @Override
                        public void onKeyEntered(final String key, GeoLocation location) {
                            if(IsAdditioAllowed(key)) {
                                Users.getUserInfo(key,new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        final Users user= dataSnapshot.getValue(Users.class);
                                        NearByPersonEntity newperson=new NearByPersonEntity(key,user.getDisplayname(),user.getPhotourl());
                                        UsersChats chats=new UsersChats(key);
                                        chats.getUsersChats().addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                String getvalue="";
                                                newperson.setChatAllowed(false);
                                                for (DataSnapshot item:dataSnapshot.getChildren()) {
                                                    if(item.getKey().equals(mAuth.getCurrentUser().getUid())) {
                                                        newperson.setChatAllowed(true);
                                                        getvalue=item.getValue(String.class);
                                                    }
                                                }
                                                if(newperson.getChatAllowed()) {
                                                    String finalGetvalue = getvalue;
                                                    newperson.setAction(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            Intent intent=new Intent(getActivity(), ChatActivity.class);
                                                            Bundle bundle=new Bundle();
                                                            bundle.putString("chatId", finalGetvalue);
                                                            bundle.putSerializable("user2",user);
                                                            intent.putExtras(bundle);
                                                            getActivity().startActivity(intent);
                                                        }
                                                    });
                                                    allpersons.add(newperson);
                                                    adapter.notifyDataSetChanged();
                                                    refreshLayout_nearby.setRefreshing(false);
                                                } else {
                                                    final UserLikes like=new UserLikes(key);
                                                    like.getReceiver(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            ArrayList<String> data=(ArrayList<String>)dataSnapshot.getValue();
                                                            ArrayList<String> receiverId=new ArrayList<>();
                                                            if(data==null) {
                                                                like.setReceiverId(receiverId);
                                                                return;
                                                            }
                                                            for (String id:data) {
                                                                receiverId.add(id);
                                                            }
                                                            like.setReceiverId(receiverId);
                                                            if(!like.getReceiverId().contains(mAuth.getCurrentUser().getUid())) {
                                                                newperson.setActionEnabled(true);
                                                            }else {
                                                                newperson.setActionEnabled(false);
                                                            }
                                                            newperson.setAction(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    like.SendLike(mAuth.getCurrentUser().getUid(), new DatabaseReference.CompletionListener() {
                                                                        @Override
                                                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                                                            newperson.setActionEnabled(false);
                                                                            try {
                                                                                Constants.SendNotification(user.getToken(),Constants.MESSAGE_TYPE_LIKE);
                                                                            } catch (JSONException e) {

                                                                            }
                                                                            Toast.makeText(getActivity(),"Liked.",Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                            allpersons.add(newperson);
                                                            adapter.notifyDataSetChanged();
                                                            refreshLayout_nearby.setRefreshing(false);
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                            refreshLayout_nearby.setRefreshing(false);
                                                        }
                                                    });
                                                }

                                            }
                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {
                                                refreshLayout_nearby.setRefreshing(false);
                                            }
                                        });


                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        refreshLayout_nearby.setRefreshing(false);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onKeyExited(final String key) {
                            for (NearByPersonEntity marker:allpersons)
                            {
                                if(key.equals(marker.getUserid()))
                                {
                                    allpersons.remove(marker);
                                }
                            }
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onKeyMoved(String key, GeoLocation location) {
                            adapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onGeoQueryReady() {

                        }

                        @Override
                        public void onGeoQueryError(DatabaseError error) {

                        }
                    });
                }
            });
        }
    }
}
