package com.gmail.rashjohar0007.happn2.basic;

import android.app.Activity;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQueryEventListener;


public interface IUserLocations {
    public void ChangeLocation(Activity activity, GeoLocation location, GeoFire.CompletionListener task);
    public void ChangeLocation(Activity activity, GeoFire.CompletionListener task);
    public void FindNearByUser(Activity activity, GeoLocation location, double radius, GeoQueryEventListener task);
    public void FindNearByUser(Activity activity, double radius, GeoQueryEventListener task);
}
