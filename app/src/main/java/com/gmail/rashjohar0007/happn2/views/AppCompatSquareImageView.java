package com.gmail.rashjohar0007.happn2.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class AppCompatSquareImageView extends AppCompatImageView{

    public AppCompatSquareImageView(Context context) {
        super(context);
    }

    public AppCompatSquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AppCompatSquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec,widthMeasureSpec);
    }
}
