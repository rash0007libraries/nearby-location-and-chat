package com.gmail.rashjohar0007.happn2.models;

public class Channels {
    private String id;
    private String sender;
    private String receiver;

    public Channels(String sender, String receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }
}
