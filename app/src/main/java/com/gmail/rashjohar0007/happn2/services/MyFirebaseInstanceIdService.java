package com.gmail.rashjohar0007.happn2.services;

import android.support.annotation.NonNull;
import android.util.Log;

import com.gmail.rashjohar0007.happn2.models.Users;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private static final String FRIENDLY_ENGAGE_TOPIC = "friendly_engage";


    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        FirebaseUser user=FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference refer= FirebaseDatabase.getInstance().getReference().child(Users.class.getSimpleName());
        if(user!=null) {
            refer.child(user.getUid()).child("token").setValue(token).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });
        }
        //FirebaseMessaging.getInstance().subscribeToTopic(token);
    }
}
