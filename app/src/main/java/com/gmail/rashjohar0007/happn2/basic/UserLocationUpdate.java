package com.gmail.rashjohar0007.happn2.basic;

import android.app.Activity;
import android.location.Location;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Johars on 1/14/2017.
 */

public class UserLocationUpdate extends UserLocation implements IUserLocations {
    @Exclude
    private FirebaseAuth mAuth;
    @Exclude
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Exclude
    private DatabaseReference myRef=database.getReference().child(UserLocationUpdate.class.getSimpleName());
    @Exclude
    private GeoFire geoFire = new GeoFire(myRef);
    @Exclude
    private GeoQuery geoQuery;
    public UserLocationUpdate() {
    }
    public UserLocationUpdate(String userid) {
        this.setUserid(userid);
    }
    public UserLocationUpdate(String userid,GeoLocation location) {
        super(userid, location);
    }
    public UserLocationUpdate(String userid,Location location) {
        super(userid, new GeoLocation(location.getLatitude(),location.getLongitude()));
    }
    public UserLocationUpdate(String userid,double latitude,double longitude) {
        super(userid,new GeoLocation(latitude,longitude));
    }
    @Override
    public void ChangeLocation(Activity activity, GeoLocation location, GeoFire.CompletionListener task) {
        geoFire.setLocation(userid, location, task);
    }

    @Override
    public void ChangeLocation(Activity activity, GeoFire.CompletionListener task) {
        geoFire.setLocation(this.getUserid(),this.getLocation(),task);
    }

    @Override
    public void FindNearByUser(Activity activity, GeoLocation location, double radius, GeoQueryEventListener task) {
        geoQuery=geoFire.queryAtLocation(location,radius);
        geoQuery.addGeoQueryEventListener(task);
    }

    @Override
    public void FindNearByUser(Activity activity, double radius, GeoQueryEventListener task) {
        geoQuery=geoFire.queryAtLocation(this.getLocation(),radius);
        geoQuery.addGeoQueryEventListener(task);
    }
}
