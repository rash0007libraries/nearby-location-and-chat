package com.gmail.rashjohar0007.happn2.basic;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.UUID;

@IgnoreExtraProperties
public class UsersChats implements Serializable{
    private String user1;
    private String user2;
    private String chatId;
    @Exclude
    private DatabaseReference reference= FirebaseDatabase.getInstance().getReference()
            .child(UsersChats.class.getSimpleName());

    public UsersChats() {
    }
    public UsersChats(String user1) {
        this.user1=user1;
        this.reference=reference.child(user1);
    }
    public UsersChats(String user1, String user2) {
        this.user1 = user1;
        this.user2 = user2;
        UUID uid=UUID.randomUUID();
        chatId=uid.toString();
    }

    public String getUser1() {
        return user1;
    }

    public void setUser1(String user1) {
        this.user1 = user1;
    }

    public String getUser2() {
        return user2;
    }

    public void setUser2(String user2) {
        this.user2 = user2;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void SaveChat(DatabaseReference.CompletionListener action){
        reference.child(user1).child(user2).setValue(chatId, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                reference.child(user2).child(user1).setValue(chatId,action);
            }
        });
    }
    public DatabaseReference getUsersChats() {
        return reference;
    }
}